package android.ceballos.crystalball;

/**
 * Created by Student on 8/26/2015.
 */
public class Predictions {
    public static Predictions predictions;
    private String[] answers;

    private Predictions(){
        answers = new String[]{
                "Your wishes came true xD  ",
                "Your wishes will never ever ever come back better"
        };

    }
    public static Predictions get(){
        if(predictions == null){
            predictions = new Predictions();
        }
        return predictions;
    }
    public String getPrediction(){
        return answers[0];
    }
}
